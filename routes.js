module.exports = [
  { 
    method: 'GET', 
    path: '/demo/01-nocache', 
    handler: (request, h) => {
      return h.file('./img/blue-sky-1.jpg')
        .header('Cache-Control', 'no-cache, no-store');
    } 
  },
  { 
    method: 'GET', 
    path: '/demo/02-maxage', 
    handler: (request, h) => {
      return h.file('./img/blue-sky-1.jpg')
        .header('Cache-Control', 'max-age=10');
    } 
  },
  { 
    method: 'GET', 
    path: '/demo/03-maxage-alt', 
    handler: (request, h) => {
      /* replace image every 20 seconds */
      let imageSn = Math.floor((new Date().getSeconds()+1) / 20) + 1;
      let img = `./img/blue-sky-${imageSn}.jpg`;

      return h.file(img)
        .header('Cache-Control', 'max-age=10');
    } 
  },
  { 
    method: 'GET', 
    path: '/demo/04-stale-while-revalidate', 
    handler: (request, h) => {
      /* replace image every 20 seconds */
      let imageSn = Math.floor((new Date().getSeconds()+1) / 20) + 1;
      let img = `./img/blue-sky-${imageSn}.jpg`;

      return h.file(img)
        .header('Cache-Control', 'max-age=5, stale-while-revalidate=10');
    } 
  },
  { 
    method: 'GET', 
    path: '/demo/05-immutable', 
    handler: (request, h) => {
      return h.file('./img/blue-sky-1.jpg')
        .header('Cache-Control', 'max-age=10, immutable');
    } 
  },
  { 
    method: 'GET', 
    path: '/demo/06-client-cache', 
    handler: (request, h) => {
      /* replace image every 20 seconds */
      let imageSn = Math.floor((new Date().getSeconds()+1) / 20) + 1;
      let img = `./img/blue-sky-${imageSn}.jpg`;

      return h.file(img)
        .header('Cache-Control', 'max-age=10');
    } 
  },
  { 
    method: 'GET', 
    path: '/demo/07-preload', 
    handler: (request, h) => {
      return h.file('./img/blue-sky-2.jpg')
        .header('Cache-Control', 'max-age=10');
    } 
  },
  {
    method: 'GET',
    path: '/{param*}',
    handler: {
      directory: {
        path: './html'
      }
    }
  },
  {
    method: 'GET',
    path: '/img/{param*}',
    handler: {
      directory: {
        path: './images'
      }
    }
  },
  {
    method: 'GET',
    path: '/js/{param*}',
    handler: {
      directory: {
        path: './js'
      }
    }
  },
];

